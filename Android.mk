ifeq ($(TARGET_DEVICE),daisy)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#----------------------------------------------------------------------
# Radio image
#----------------------------------------------------------------------
#ifeq ($(ADD_RADIO_FILES), true)
#radio_dir := $(LOCAL_PATH)/radio
#RADIO_FILES := $(shell cd $(radio_dir) ; ls)
#$(foreach f, $(RADIO_FILES), \
#    $(call add-radio-file,radio/$(f)))
#endif

INSTALLED_RADIOIMAGE_TARGET += $(TARGET_BOOTLOADER_EMMC_INTERNAL)
$(call add-radio-file,splash.img)
$(call add-radio-file,modem.img)
$(call add-radio-file,rpm.img)
$(call add-radio-file,sbl1.img)
$(call add-radio-file,dsp.img)
$(call add-radio-file,cmnlib.img)
$(call add-radio-file,cmnlib64.img)
$(call add-radio-file,keymaster.img)
$(call add-radio-file,tz.img)
$(call add-radio-file,devcfg.img)
$(call add-radio-file,mdtp.img)
$(call add-radio-file,aboot.img)

endif